# Django Tutorial
### Focused on django basics, ORM and django-ninja

## Instruction
Start with videos. Final source code and related docs are added.

## Videos:
1. django-ninja-showcase: https://www.youtube.com/watch?v=V88rdXhwneg
2. python tutorial: https://www.youtube.com/watch?v=t8pPdKYpowI
3. django-tutorial-1: https://www.youtube.com/watch?v=OX9ka5fyIkU
4. django-tutorial-2: https://www.youtube.com/watch?v=RGDjHaLu9G4
5. django-tutorial-3: https://www.youtube.com/watch?v=zEd6gh45rWM
6. Git, VS Code, Gitlab: https://www.youtube.com/watch?v=27RH999xt-c

### Requirements:
- VScode
- Python
- Git (optional)

### Vscode extensions:
- Pylance: https://marketplace.visualstudio.com/items?itemName=ms-python.vscode-pylance
- Remote Development: https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack
- Codeium: https://marketplace.visualstudio.com/items?itemName=Codeium.codeium
- Sqlite Viewer: https://marketplace.visualstudio.com/items?itemName=qwtel.sqlite-viewer

### commands:
- python -m venv .\path\to\myenv
- source <venv>/bin/activate
- .\<venv>\Scripts\Activate.ps1

- pip install -r requirements.txt

- django-admin startproject tutorial
- python manage.py startapp bookstore

- python manage.py makemigrations
- python manage.py migrate

- python manage.py runserver
- python manage.py createsuperuser

## Useful Docs:
- venv: https://docs.python.org/3/library/venv.html

- Django: https://docs.djangoproject.com/en/5.0/intro/tutorial01/
- Django Girls: https://tutorial.djangogirls.org/en/

- Django Field Types: https://docs.djangoproject.com/en/5.0/ref/models/fields/#field-types
- Django Field Options: https://docs.djangoproject.com/en/5.0/ref/models/fields/#field-options

- Django Query Tutorial Doc: https://docs.djangoproject.com/en/5.0/topics/db/queries/
- Django Query Reference Doc: https://docs.djangoproject.com/en/5.0/ref/models/querysets/
- Django Query Field Lookup: https://docs.djangoproject.com/en/5.0/ref/models/querysets/#field-lookups
- Django ORM Example: https://python.plainenglish.io/django-orm-examples-and-practice-problems-7896e9906855

- Django Ninja: https://django-ninja.dev/
- Django Ninja JWT: https://eadwincode.github.io/django-ninja-jwt/

- Django Ninja - JWT Example: 
    - https://github.com/lihuacai168/django-ninja-demo
    - https://github.com/ErSauravAdhikari/Karnali
    - https://github.com/mohammadanarul/django-ninja-api

