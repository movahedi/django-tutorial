from datetime import date
from typing import List

from django.shortcuts import render

from ninja import ModelSchema, Router, Schema
from ninja.pagination import paginate
from ninja_jwt.authentication import JWTAuth

from bookstore import models
from bookstore import schemas

# Create your views here.

# Router of django-ninja
router = Router(auth=JWTAuth())


@router.get("/hello")
def hello(request):
    """
    This docstring will be visible in the OpenAPI schema  
    Use for endpoint documentation   
    You can use markdown syntax if you prefer  
    """
    return f"Hello {request.user.username}"

@router.get("/add", auth=None)
def add(request, a: int, b: int):
    return {"result": a + b}


@router.post("/multiply", auth=None)
def multiply(request, a: int, b: int):
    return {"result": a * b}


@router.post("/params/{path_param}")
def params(request, path_param: int, item: schemas.ParamBody,  q: int = None):
    return { "path_param": path_param , "q": q}


@router.get("/publisher", response=List[schemas.PublisherOut])
@paginate
def publishers(request, name: str = None):
    if name:
       return models.Publisher.objects.filter(name__startswith=name)
    else:
        return models.Publisher.objects.all()


@router.post('/publisher', response=schemas.PublisherOut)
def create_publisher(request, publisher: schemas.PublisherIn):
    # publisher = models.Publisher.objects.create(name=publisher.name)
    created_publisher = models.Publisher.objects.create(**publisher.dict())
    return created_publisher



@router.get("/book", response=List[schemas.BookOut])
def get_book(request):
    return models.Book.objects.all()

@router.post("/book", response=schemas.BookOut)
def create_book(request, body: schemas.BookIn):
    book = models.Book.objects.create(**body.dict())
    return book


@router.post("/review", response=schemas.ReviewResponse)
def create_review(request, body: schemas.ReviewRequestBody):
    review = models.Review.objects.create(
                text = body.text,
                date = date.today(),
                book_id = body.book_id,
                user_id = request.user.id
            )
    return review


@router.get("/review", response=List[schemas.ReviewResponse])
@paginate
def get_reviews(request, book_id: int):
    return models.Review.objects.filter(book_id=book_id)

@router.delete("/review")
def delete_review(request, review_id: int):
    review = models.Review.objects.get(id=review_id)
    if review.user_id == request.user.id:
        review.delete()
        return {"Message": "Deleted."}
    else:
        return {"Message": "Not allowed."}
