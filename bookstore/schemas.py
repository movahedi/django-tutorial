

from ninja import ModelSchema, Schema

from bookstore import models


class ParamBody(Schema):
    name: str
    age: int



class PublisherIn(ModelSchema):
    class Meta:
        model = models.Publisher
        fields = ['name']
        fields_optional = '__all__'


class PublisherOut(ModelSchema):
    class Meta:
        model = models.Publisher
        fields = "__all__"



class BookIn(ModelSchema):
    class Meta:
        model = models.Book
        fields = "__all__"
        exclude = ['id']

class BookOut(ModelSchema):
    class Meta:
        model = models.Book
        fields = "__all__"
    
    publisher: PublisherOut


class ReviewRequestBody(Schema):
    text: str
    book_id: int

class ReviewResponse(ModelSchema):
    class Meta:
        model = models.Review
        fields = "__all__"

