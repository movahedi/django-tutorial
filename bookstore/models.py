from django.db import models

# Create your models here.


class Publisher(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name



class Book(models.Model):
    name = models.CharField(null=False, max_length=250)
    author = models.CharField(max_length=250)
    page_count = models.IntegerField(default=0)
    publisher = models.ForeignKey(to=Publisher, on_delete=models.CASCADE, null=True)
    publish_date = models.DateField(null=True)
    price = models.FloatField(default=0.0)

    def __str__(self):
        return f"{self.name} - {self.author}"

    class Meta:
        unique_together = [("publisher", "name")]


class Review(models.Model):
    text = models.TextField()
    date = models.DateField()
    book = models.ForeignKey(to=Book, on_delete=models.CASCADE)
    user = models.ForeignKey(to="auth.User", on_delete=models.SET_NULL, null=True)
