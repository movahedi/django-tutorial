import datetime
from models import *


# Tutorial Doc: https://docs.djangoproject.com/en/5.0/topics/db/queries/
# Refrence Doc: https://docs.djangoproject.com/en/5.0/ref/models/querysets/
# Field Lookup: https://docs.djangoproject.com/en/5.0/ref/models/querysets/#field-lookups

# create a publisher
Publisher.objects.create(namespace_id=0, name="Elsivier")

# create a publisher
p = Publisher(namespace_id=0, name="Elsivier")
p.save()

# query a publisher by get or filter
p = Publisher.objects.get(id=1)
p = Publisher.objects.filter(namespace_id=0, name="Elsivier").first()

# create a Book
Book.objects.create(publisher=p,
                    name="Internal Medical Book",
                    author="Harrison", 
                    page_count=3500, 
                    publish_date=datetime.date(2019, 1, 1),
                    price=10.0)

Book.objects.create(publisher_id=1,
                    name="Internal Medical Book",
                    author="Harrison", 
                    page_count=3500, 
                    publish_date=datetime.date(2019, 1, 1),
                    price=10.0)


# Edit a publisher (get object first then edit)
p = Publisher.objects.get(id=1)
p.name = "Springer"
p.save()

# Edit a publisher (update in one query)
Publisher.objects.filter(id=1).update(name="Springer")

# Delete a publisher
p = Publisher.objects.get(id=1)
p.delete()

# Delete a book
b = Book.objects.get(id=1)
b.delete()

# Edit or create a publisher
p = Publisher.objects.update_or_create(
    namespace_id=0,
    name="Springer",
)
id = p[0].id # get id of edited/created publisher
